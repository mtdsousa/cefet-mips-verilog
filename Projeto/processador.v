module processador(input clock, input clear);
  //PC
  /*module pc(
    input [4:0]pcin,
    input clear,
    input clock,
    output reg [4:0]pcout);
  */
  wire [4:0] pcin;
  wire [4:0] pcout;
  pc PC(pcin, clear, clock, pcout);
  
  //Instruction Memory
  /*module memoriainstrucao(
    input [4:0]readaddress,
    input [31:0]instruction);
  */
  wire [31:0] Instruction;
  memoriainstrucao InstructionMemory(pcout, clock, Instruction);
  
  //Control
  /*module control(
    input [5:0]op,
    output regDst,
    output aluSrc,
    output memtoReg,
    output regWrite,
    output memRead,
    output memWrite, 
    output branch,
    output [1:0]aluOp);
  */
  wire RegDst;
  wire Branch;
  wire MemRead;
  wire MemtoReg;
  wire [1:0]ALUOp;
  wire MemWrite;
  wire ALUSrc;
  wire RegWrite;
  control Control(Instruction[31:26], RegDst, ALUSrc, MemtoReg, RegWrite, MemRead, MemWrite, Branch, ALUOp);
  
  //Registers
  /*module bancoregistradores(
    input [4:0]readregister1,
    input [4:0]readregister2,
    input [4:0]writeregister,
    input [31:0]writedata,
    input regWrite,
    output [31:0]readdata1,
    output [31:0]readdata2);
  */
  wire [4:0]m0out;
  wire [31:0]WriteData;
  wire [31:0]ReadData1;
  wire [31:0]ReadData2;
  mux5bits2p1 m0(Instruction[20:16], Instruction[15:11], RegDst, m0out);
  bancoregistradores Registers(Instruction[25:21], Instruction[20:16], m0out, WriteData, RegWrite, clock, ReadData1, ReadData2);
  
  //Sign Extend
  /*module extensor16p32(
    input [15:0]in,
    output [31:0]out);
  */
  wire [31:0]SignExtendOut;
  extensor16p32 SignExtend(Instruction[15:0], SignExtendOut);
  
  //ALU Control
  /*module aluControl(
    input [5:0]f,
    input [1:0]aluOp,
    output [2:0]operation);
  */
  wire [2:0]ALUControlOut;
  aluControl ALUControl(Instruction[5:0], ALUOp, ALUControlOut);
  
  //ALU
  /*module ULA(
    input [31:0]a,
    input [31:0]b,
    input [2:0]operation,
    output reg zero,
    output [31:0]result);
  */
  wire [31:0]m1out;
  wire zero;
  wire [31:0] ALUResult;
  mux32bits2p1 m1(ReadData2, SignExtendOut, ALUSrc, m1out);
  ULA ALU(ReadData1, m1out, ALUControlOut, zero, ALUResult);
  
  //ShiftLeft 2
  /*module shiftLeft(
    input [31:0]a,
    output [31:0]b);
  */
  wire [31:0]ShiftLeftOut;
  shiftLeft ShiftLeft(SignExtendOut, ShiftLeftOut);
  
  //sa
  /*module somador5bits(
    input [4:0]a,
    input [4:0]b,
    input ci,
    output co,
    output [4:0] s);
  */
  wire saco;
  wire [4:0]saOut;
  somador5bits sa(pcout, 5'b100, 1'b0, saco, saOut);
  
  //sb
  /*module somador32bits(
    input [31:0]a,
    input [31:0]b,
    input ci,
    output co,
    output [31:0] s);
  */
  wire sbco;
  wire [31:0] sbOut;
  wire [31:0] saOut32;
  assign saOut32 = {27'b0, saOut};
  somador32bits sb(ShiftLeftOut, saOut32, 1'b0, sbco, sbOut);
  mux5bits2p1 m2(saOut, sbOut[4:0], (Branch & zero), pcin);
  
  //Data Memory
  /* module memoriadados(
    input [4:0]address,
    input[31:0]writedata,
    input memWrite,
    input memRead,
    output [31:0]readdata);
  */
  wire [31:0]ReadData;
  memoriadados DataMemory(ALUResult[4:0], ReadData2, MemWrite, MemRead, ReadData);
  mux32bits2p1 m3(ALUResult, ReadData, MemtoReg, WriteData);
endmodule
