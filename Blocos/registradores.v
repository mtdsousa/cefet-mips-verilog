module FFD(D, enable, clk, Q);
    input D, clk, enable;
    output reg Q;
  
    initial begin
        Q = 1'b0;
    end
    
    always@(posedge clk)
      if (clk&enable)
        Q = D;
endmodule

module reg32bits(input [31:0]in, output [31:0]out, input enable, input clk);
  genvar i;
  generate
    for (i=0; i <=31; i=i+1) begin : FFD
      FFD f(in[i], enable, clk, out[i]);
    end
  endgenerate
endmodule

module bancoregistradores(input [4:0]readregister1, input [4:0]readregister2, 
                          input [4:0]writeregister, input [31:0]writedata,
                          input regWrite,           input clk,
                          output [31:0]readdata1,   output [31:0]readdata2);
  
  wire [31:0] regOut [0:31];
  wire [31:0]d1Out;                       
  Decode d1(writeregister, d1Out);
  
  genvar i;
  generate
    for(i=0; i<32; i=i+1) begin : registradores
      reg32bits r(writedata, regOut[i], d1Out[i]&regWrite, clk);
    end
  endgenerate
  
  mux32bits32p1 m1(regOut[0], regOut[1], regOut[2], regOut[3], regOut[4],
                   regOut[5], regOut[6], regOut[7], regOut[8], regOut[9], 
                   regOut[10], regOut[11], regOut[12], regOut[13], regOut[14],
                   regOut[15], regOut[16], regOut[17], regOut[18], regOut[19],
                   regOut[20], regOut[21], regOut[22], regOut[23], regOut[24],
                   regOut[25], regOut[26], regOut[27], regOut[28], regOut[29],
                   regOut[30], regOut[31], readregister1, readdata1);
                   
  mux32bits32p1 m2(regOut[0], regOut[1], regOut[2], regOut[3], regOut[4],
                   regOut[5], regOut[6], regOut[7], regOut[8], regOut[9], 
                   regOut[10], regOut[11], regOut[12], regOut[13], regOut[14],
                   regOut[15], regOut[16], regOut[17], regOut[18], regOut[19],
                   regOut[20], regOut[21], regOut[22], regOut[23], regOut[24],
                   regOut[25], regOut[26], regOut[27], regOut[28], regOut[29],
                   regOut[30], regOut[31], readregister2, readdata2); 
  
  /*reg [31:0] regs [0:31];
  integer i;
  initial begin
    //Iniciando valores para os registradores 17 e 18 (18 e 2 respectivamente).
    regs[17] <= 32'b10010;
    regs[18] <= 32'b10;
    
    for (i=0; i < 32; i=i+1) begin : clear
        if(i!=17 & i!=18)
          regs[i] <= 32'b0;      
    end
  end
  
  assign readdata1 = regs[readregister1];
  assign readdata2 = regs[readregister2];
  
  always@(*) begin
    if(regWrite)
      regs[writeregister] <= writedata;
  end*/
endmodule
