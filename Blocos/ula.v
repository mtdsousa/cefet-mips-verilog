
/*
Refer�ncias:
https://cs.nyu.edu/~gottlieb/courses/2007-08-fall/arch/lectures/lecture-04.html
https://cs.nyu.edu/courses/fall01/V22.0436-001/lectures/lecture-09.html
http://www.cs.trinity.edu/~jhowland/cs2321/lab6/
*/

module ULA(input [31:0]a, input [31:0]b, input [2:0]operation, output reg zero, output [31:0]result);
  wire overflow;
  wire co;
  
  //Entrada b invertida se necess�rio
  wire [31:0]nb;
  mux32bits2p1 mnb(b, ~b, operation[2], nb);
  
  //Opera��es AND e OR
  wire [31:0]opand;
  wire [31:0]opor;
  genvar i;
  generate
    for(i=0; i<32; i=i+1) begin
      assign opand[i] = a[i]&nb[i];
      assign opor[i] = a[i]|nb[i];
    end
  endgenerate
  
  //Opera��o de soma/subtra��o
  wire [31:0]opadd;
  somador32bits s(a, nb, operation[2], co, opadd);
  
  //Opera��o set on less than
  wire [31:0]opstl;
  assign opstl[0] = opadd[31];
  generate
    for(i=1; i<32; i=i+1) begin
      assign opstl[i] = 1'b0;
    end
  endgenerate
  
  //Atribuindo valor ao resultado a partir da instru��o
  mux32bits4p1 m(opand, opor, opadd, opstl, operation[1:0], result);
  
  //Verificando resultado zero
  integer j;
  always@(*) begin
    zero <= 1'b1;
    for(j=0; j<32; j=j+1)
      if(result[j] == 1'b1)
        zero <= 1'b0;
  end
endmodule