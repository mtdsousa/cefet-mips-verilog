module mux2p1(input a, input b, input s, output r);
  assign r = (a&~s)|(b&s);
endmodule

module mux5bits2p1(input [4:0]a, input [4:0]b, input seletor, output [4:0]out);
  mux2p1 m1(a[0], b[0], seletor, out[0]);
  mux2p1 m2(a[1], b[1], seletor, out[1]);
  mux2p1 m3(a[2], b[2], seletor, out[2]);
  mux2p1 m4(a[3], b[3], seletor, out[3]);
  mux2p1 m5(a[4], b[4], seletor, out[4]);
endmodule

module mux32bits2p1(input [31:0]a, input[31:0]b, input s, output [31:0]r);
  genvar i;
  generate
      for(i=0; i<32; i=i+1) begin : mux2p1
         mux2p1 m(a[i], b[i], s, r[i]);
      end
  endgenerate
endmodule

module mux4p1(input [0:3]in, input [1:0]s, output r);
  mux2p1 m1(in[0], in[1], s[0], rm1);
  mux2p1 m2(in[2], in[3], s[0], rm2);
  mux2p1 m3(rm1, rm2, s[1], r);
endmodule

module mux32bits4p1(input [31:0]a, input [31:0]b, input [31:0]c, input [31:0]d, input [1:0]s, output [31:0]r);
  genvar i;
  generate
    for(i=0; i<32; i=i+1) begin : mux4p1
      mux4p1 m({a[i], b[i], c[i], d[i]}, s, r[i]);
    end
  endgenerate
endmodule

module mux32p1(input [31:0]in, input [4:0]s, output out);
  wire [15:0]outa;
  wire [7:0]outb;
  wire [3:0]outc;
  wire [1:0]outd;
  
  genvar i;
  generate
    for(i=0; i<16; i=i+1) begin : a 
      mux2p1 ma(in[i*2], in[(i*2)+1], s[0], outa[i]);
    end
    
    for(i=0; i<8; i=i+1) begin : b
      mux2p1 mb(outa[i*2], outa[(i*2)+1], s[1], outb[i]);
    end
    
    for(i=0; i<4; i=i+1) begin : c
      mux2p1 mc(outb[i*2], outb[(i*2)+1], s[2], outc[i]);
    end
    
    for(i=0; i<2; i=i+1) begin : d
      mux2p1 md(outc[i*2], outc[(i*2)+1], s[3], outd[i]);
    end
    
    mux2p1 m1(outd[0], outd[1], s[4], out);
  endgenerate
endmodule

module mux32bits32p1(input [31:0]in1, input [31:0]in2, input [31:0]in3, 
input [31:0]in4, input [31:0]in5, input [31:0]in6, input [31:0]in7, input [31:0]in8, 
input [31:0]in9, input [31:0]in10,input [31:0]in11, input [31:0]in12, input [31:0]in13, 
input [31:0]in14, input [31:0]in15, input [31:0]in16, input [31:0]in17, input [31:0]in18, 
input [31:0]in19, input [31:0]in20, input [31:0]in21, input [31:0]in22, input [31:0]in23, 
input [31:0]in24, input [31:0]in25, input [31:0]in26, input [31:0]in27, input [31:0]in28, 
input [31:0]in29, input [31:0]in30, input [31:0]in31, input [31:0]in32, input [4:0]s, output [31:0]out); 
    genvar i;
    generate
        for (i=0; i <=31; i=i+1) begin : mux32p1
            mux32p1 m({in32[i], in31[i], in30[i], 
            in29[i], in28[i], in27[i], in26[i], in25[i],
            in24[i], in23[i], in22[i], in21[i], in20[i], 
            in19[i], in18[i], in17[i], in16[i], in15[i], 
            in14[i], in13[i], in12[i], in11[i], in10[i], 
            in9[i], in8[i], in7[i], in6[i], in5[i], 
            in4[i], in3[i], in2[i], in1[i]}, s, out[i]);
        end
    endgenerate
endmodule