module memoriainstrucao(input [4:0]readaddress, input clock, output reg [31:0]instruction);
  reg [7:0] memory [0:31];
  integer i;
  initial begin
    //lw $7, 6($9)
    //100011 01001 00111 0000000000000110
    memory[0] <= 8'b10001101;
    memory[1] <= 8'b00100111;
    memory[2] <= 8'b00000000;
    memory[3] <= 8'b00000110;
    
    //lw $8, 2($9)
    //100011 01.001 01000. 00000000.00000010
    memory[4] <= 8'b10001101;
    memory[5] <= 8'b00101000;
    memory[6] <= 8'b00000000;
    memory[7] <= 8'b00000010;
    
    //add $8, $8, $7
    //000000 01.000 00111. 01000 000.00 100000
    memory[8] <= 8'b00000001;
    memory[9] <= 8'b00000111;
    memory[10] <= 8'b01000000;
    memory[11] <= 8'b00100000;
    
    //sw $8, 2($9)
    //101011 01.001 01000. 00000000.00000010
    memory[12] <= 8'b10101101;
    memory[13] <= 8'b00101000;
    memory[14] <= 8'b00000000;
    memory[15] <= 8'b00000010;
    
    //beq $8, $9, -5
    //000100 01.000 01001. 11111111.11111011
    memory[16] <= 8'b00010001;
    memory[17] <= 8'b00001001;
    memory[18] <= 8'b11111111;
    memory[19] <= 8'b11111011;
    
    for (i=20; i < 32; i=i+1) begin : clear
        memory[i] <= 8'b0;      
    end
  end
  
  always@(*) begin
    instruction[31:24] <= memory[readaddress];
    instruction[23:16] <= memory[readaddress+1];
    instruction[15:8]  <= memory[readaddress+2];
    instruction[7:0]   <= memory[readaddress+3];
  end
endmodule

module memoriadados(input [4:0]address, input[31:0]writedata, input memWrite, input memRead, output reg [31:0]readdata);
  reg [7:0] memory [0:31];
  integer i;
  initial begin
    memory[0] <= 8'b0;
    memory[1] <= 8'b0;
    //2: decimal(10)
    memory[2] <= 8'b0;
    memory[3] <= 8'b0;
    memory[4] <= 8'b0;
    memory[5] <= 8'b1010;
    //6: decimal (-10)
    memory[6] <= 8'b11111111;
    memory[7] <= 8'b11111111;
    memory[8] <= 8'b11111111;
    memory[9] <= 8'b11110110;
    
    for (i=10; i < 32; i=i+1) begin : clear
        memory[i] <= 8'b0;      
    end
  end
  
  always@(*) begin
    if(memWrite) 
      begin
        memory[address] <= writedata[31:24];
        memory[address+1] <= writedata[23:16];
        memory[address+2] <= writedata[15:8];
        memory[address+3] <= writedata[7:0];
      end
    else
      begin
        if(memRead)
          begin
            readdata[31:24] <= memory[address];
            readdata[23:16] <= memory[address+1];
            readdata[15:8]  <= memory[address+2];
            readdata[7:0]   <= memory[address+3];
          end
      end
  end
endmodule