module pc(input [4:0]pcin, input clear, input clock, output reg [4:0]pcout);
  
  always@(posedge clock, negedge clear) begin
    if(~clear)
      pcout <= 4'b0;
    else if(clock)
      pcout <= pcin;
  end
endmodule