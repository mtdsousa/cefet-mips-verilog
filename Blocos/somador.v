module meioSomador(input a, input b, output c, output s);
  assign s = (a&~b)|(~a&b);
  assign c = (a&b);
endmodule

module somador(input a, input b, input ci, output co, output s);
  meioSomador ms1(a, b, c1, s1);
  meioSomador ms2(ci, s1, c2, s);
  assign co = (c2|c1);
endmodule

module somador5bits(input [4:0]a, input [4:0]b, input ci, output co, output [4:0] s);
  somador s1(a[0], b[0], ci, co1, s[0]);
  somador s2(a[1], b[1], co1, co2, s[1]);
  somador s3(a[2], b[2], co2, co3, s[2]);
  somador s4(a[3], b[3], co3, co4, s[3]);
  somador s5(a[4], b[4], co4, co, s[4]);
endmodule

module somador32bits(input [31:0]a, input [31:0]b, input ci, output co, output [31:0] s);
  wire [31:0]cout;
  genvar i;
  generate
    somador s0(a[0], b[0], ci, cout[0], s[0]);
    for (i=1; i < 32; i=i+1) begin : somador
        somador s(a[i], b[i], cout[i-1], cout[i], s[i]);
    end
  endgenerate
  assign co = cout[31];
endmodule