//Fonte: http://objectmix.com/verilog/189722-16-32-bit-sign-extention.html
module extensor16p32(in, out);
  input [15:0] in;
  output [31:0] out;
  
  assign out = {{16{in[15]}}, in[15:0]};
endmodule

/*module extensor5p32(in, out);
  input [4:0] in;
  output [31:0] out;
  
  assign out = {{5{in[4]}}, in[4:0]};
endmodule*/
