module aluControl(input [5:0]f, input [1:0]aluOp, output [2:0]operation);
  assign operation[2] = (aluOp[0] | (aluOp[1]&f[1]));
  assign operation[1] = ~(aluOp[1]&f[2]);
  assign operation[0] = (aluOp[1]&(f[3]|f[0]));
endmodule

module control(input [5:0]op, output regDst, output aluSrc, output memtoReg, output regWrite, output memRead, output memWrite, output branch, output [1:0]aluOp);
  wire Rformat;
  wire lw;
  wire sw;
  wire beq;
  
  assign Rformat = (~op[0] & ~op[1] & ~op[2] & ~op[3] & ~op[4] & ~op[5]);
  assign lw = (op[0] & op[1] & ~op[2] & ~op[3] & ~op[4] & op[5]);
  assign sw = (op[0] & op[1] & ~op[2] & op[3] & ~op[4] & op[5]);
  assign beq = (~op[0] & ~op[1] & op[2] & ~op[3] & ~op[4] & ~op[5]);
  
  assign regDst = Rformat;
  assign aluSrc = (lw | sw);
  assign memtoReg = lw;
  assign regWrite = (Rformat | lw);
  assign memRead = lw;
  assign memWrite = sw;
  assign branch = beq;
  assign aluOp[1] = Rformat;
  assign aluOp[0] = beq;
endmodule
